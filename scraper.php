<?php
if (function_exists('xdebug_disable')) {
	xdebug_disable();
}
error_reporting(E_ALL & ~E_NOTICE);
date_default_timezone_set(@date_default_timezone_get());
require_once __DIR__.'/vendor/autoload.php';

////////
require_once 'inc/simple_html_dom.php';
////////
echo '<html><head><title>Pokemon Scraper</title></head><body>';
echo '<h1>Pokemon Scraper</h1><hr>';
//$html = file_get_html('https://pokemondb.net/evolution');
$response = file_get_contents("https://pokemondb.net/evolution", false, stream_context_create(array(
	"ssl"=>array(
		"verify_peer"=>false,
		"verify_peer_name"=>false,
	),
)));
$html = str_get_html($response);
foreach($html->find('.infocard-tall') as $item1) {
	if(strpos($item1->class, 'small')!==false) continue;
	$nome = $item1->find('.ent-name', 0)->plaintext;
	$famiglie = array();
	foreach ($item1->find('.aside a') as $item2) {
		$famiglie[] = $item2->plaintext;
	}

	echo 'personaggio trovato: '.$nome."<br>\n";
	echo 'famiglie: '.implode(', ', $famiglie)."<br>\n";
	echo "<br>\n";
}
echo '</body></html>';
$html->clear();
unset($html);
